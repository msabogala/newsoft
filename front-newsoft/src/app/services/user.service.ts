import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {UserModel} from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = 'https://crudnewsoft.firebaseio.com';

  constructor( private http: HttpClient) {}

  registarUsuario(usuario:UserModel){

    return this.http.post(`${this.url}/usuarios.json`, usuario).pipe(map((resp:any)=>{
      usuario.id_usuario = resp.name;
      return usuario;
    })
    );

  }

  actualizarUsuario(usuario:UserModel){

    const usuarioTemp ={
      ...usuario
    };

    delete usuarioTemp.id_usuario;

    return this.http.put(`${this.url}/usuarios/${usuario.id_usuario}.json`, usuarioTemp);

  }

  deleteUsuario(id: string){
    return this.http.delete(`${this.url}/usuarios/${id}.json`);
  }

  getUserById(id: string) {

    return this.http.get(`${this.url}/usuarios/${id}.json`);
  }

  getusuarios(){
    return this.http.get(`${this.url}/usuarios.json`).pipe(map(resp => this.crearArreglo(resp))
    );
  }
  private crearArreglo(usuariosObj: object){
    const usuarios: UserModel[] = [];
    console.log(usuariosObj)
    Object.keys (usuariosObj).forEach(key=>{
      const usuario:UserModel = usuariosObj[key];
      usuario.id_usuario = key;
      usuarios.push(usuario);
    })
    if(usuariosObj === null){
      return [];
    }
    return usuarios;
  }
}
