export class UserModel{

    id_usuario: string;
    tipo_documento: string;
    numero_documento: number;
    nombre: string;
    apellido: string;
    fecha: Date;
    area: string;

    constructor() {
        this.id_usuario =null        
        this.tipo_documento =null;
        this.numero_documento =null;
        this.nombre =null;
        this.apellido =null;
        this.fecha =null        
        this.area =null;
    }
}