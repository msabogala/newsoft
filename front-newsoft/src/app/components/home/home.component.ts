import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../models/user.model';
import { FormGroup, FormArray, Validators, FormControl, FormControlName, NgForm } from '@angular/forms';
import { UserService } from '../../services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usuario: UserModel = new UserModel();
  updateUsuario: UserModel;
  form: FormGroup;
  buttonClicked: number;

  constructor(private usuarioService: UserService) { 
    this.form = new FormGroup({
      //id_usuario: new FormControl(''),
      tipo_documento: new FormControl('',[Validators.required, Validators.minLength(3),Validators.maxLength(20)]),
      numero_documento: new FormControl('',[Validators.required, Validators.pattern('[0-9]{7,12}')]),         
      nombre: new FormControl('', [Validators.required, Validators.minLength(3),Validators.maxLength(20)]),
      apellido: new FormControl('', [Validators.required, Validators.minLength(3),Validators.maxLength(20)]),  
      fecha: new FormControl('',Validators.required),
      area: new FormControl('', [Validators.required, Validators.minLength(3),Validators.maxLength(20)]),
    });
  }
  get tipoInvalido(){
    return this.form.get('tipo_documento').invalid && this.form.get('tipo_documento').touched
  }
  get numeroInvalido(){
    return this.form.get('numero_documento').invalid && this.form.get('numero_documento').touched
  }
  get nombreInvalido(){
    return this.form.get('nombre').invalid && this.form.get('nombre').touched
  }
  get apellidoInvalido(){
    return this.form.get('apellido').invalid && this.form.get('apellido').touched
  }
  get fechaInvalido(){
    return this.form.get('fecha').invalid && this.form.get('fecha').touched
  }
  get areaInvalido(){
    return this.form.get('area').invalid && this.form.get('area').touched
  }

  ngOnInit() {
  }
  guardar(form: NgForm){
    if (this.buttonClicked === 0){
      console.log('Accede')
      console.log(this.form.value)
     
      if(this.form.valid){
        this.usuarioService.registarUsuario(this.usuario).subscribe(resp =>{
          console.log(resp);
          console.log('1');
          Swal.fire({
            title: 'Registro exitoso',
            text: 'El usuario: '+ this.usuario.nombre +' fue guaradado de manera correcta, con número de identificación '+ this.usuario.numero_documento,
            icon: 'success'
          })
        })
      }else{
        Swal.fire({
          title: '¡Error!',
          text: 'Campos vacíos o incorrectos',
          icon: 'error'
        })
      }
    }
  }
}
