import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../../models/user.model';
import { FormGroup, FormArray, Validators, FormControl, FormControlName, NgForm } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import Swal from 'sweetalert2';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id : string;
  usuario: UserModel = new UserModel();
  updateUsuario: UserModel;
  form: FormGroup;
  buttonClicked: number;

  placeTipoDocumento: any;
  placeNumeroDocumento: any;
  placeNombre: any;
  placeApellido: any;
  placeFecha: any;
  placeArea: any;

  constructor(private usuarioService: UserService, private activatedRoute: ActivatedRoute) { 
    this.form = new FormGroup({
      //id_usuario: new FormControl(''),
      tipo_documento: new FormControl('',[Validators.required, Validators.minLength(3),Validators.maxLength(20)]),
      numero_documento: new FormControl('',[Validators.required, Validators.pattern('[0-9]{7,12}')]),         
      nombre: new FormControl('', [Validators.required, Validators.minLength(3),Validators.maxLength(20)]),
      apellido: new FormControl('', [Validators.required, Validators.minLength(3),Validators.maxLength(20)]),  
      fecha: new FormControl('',Validators.required),
      area: new FormControl('', [Validators.required, Validators.minLength(3),Validators.maxLength(20)]),
    });
  }
  get tipoInvalido(){
    return this.form.get('tipo_documento').invalid && this.form.get('tipo_documento').touched
  }
  get numeroInvalido(){
    return this.form.get('numero_documento').invalid && this.form.get('numero_documento').touched
  }
  get nombreInvalido(){
    return this.form.get('nombre').invalid && this.form.get('nombre').touched
  }
  get apellidoInvalido(){
    return this.form.get('apellido').invalid && this.form.get('apellido').touched
  }
  get fechaInvalido(){
    return this.form.get('fecha').invalid && this.form.get('fecha').touched
  }
  get areaInvalido(){
    return this.form.get('area').invalid && this.form.get('area').touched
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id']; //id_producto 
      console.log(this.id)

      this.usuarioService.getUserById(this.id)
      .subscribe((resp:UserModel) => {
        console.log(resp);
        this.usuario = resp
        this.usuario.id_usuario = this.id;
        console.log(this.usuario.nombre);
      }); 
    });
  }

  actualizar(form: NgForm){
    if (this.buttonClicked === 1){
      if(this.form.valid){
        this.usuarioService.actualizarUsuario(this.usuario).subscribe(resp =>{
          console.log(resp);
          Swal.fire({
            title: 'Actualización exitosa',
            text: 'El usuario fue actualizado de manera correcta',
            icon: 'success'
          })
        })
      }else{
        Swal.fire({
          title: '¡Error!',
          text: 'Campos vacíos o incorrectos',
          icon: 'error'
        })
      }
    }
  }
}
