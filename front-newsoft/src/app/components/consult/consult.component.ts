import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../models/user.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-consult',
  templateUrl: './consult.component.html',
  styleUrls: ['./consult.component.css']
})
export class ConsultComponent implements OnInit {

  usuarios: UserModel[]=[];

  constructor(private usuariosService: UserService) { }

  ngOnInit() {
    this.usuariosService.getusuarios().subscribe(resp=>{
      this.usuarios = resp;
      console.log(this.usuarios)
    });
  }

  borrarUsuario(usuario:UserModel, i:number){
    Swal.fire({
      title:'¿Está seguro de eliminar este usuario?',
      icon: 'question',
      showCancelButton: true,
      showConfirmButton: true,
    }).then(resp=>{
      if(resp.value){
        this.usuarios.splice(i,1);
        this.usuariosService.deleteUsuario(usuario.id_usuario).subscribe();
      }
    });
  }

}
