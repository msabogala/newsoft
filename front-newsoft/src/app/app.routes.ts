import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ConsultComponent } from './components/consult/consult.component';
import { EditComponent } from './components/consult/edit/edit.component';


const APP_ROUTES: Routes =
[ 
    { path: 'home', component: HomeComponent },
    { path: 'consult', component: ConsultComponent },
    { path: 'edit/:id', component: EditComponent },
    {path: '**', pathMatch: 'full', redirectTo: 'home'},
    {path: '', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash: true});
