# Prueba Newsoft

**Para clonar el repositorio:**[Link](https://gitlab.com/msabogala/newsoft.git/ "Repositorio Gitlab")

El reto fue desarrollado en **Angular8**, una vez clonado el repositorio para correr la app es necesario ejecutar  **npm install** y  **ng build**, desde la carperta **front-newsoft** para instalar las dependencia necesarias y seguidamente ejecutar con  **ng serve -o** para correr la app. El backend fue desplegado en firebase en una **RealTimeDataBase**. Con el siguiente link se puede consultar la base de datos [Link](https://crudnewsoft.firebaseio.com/usuarios.json/ "realtimedatabase") **https://crudnewsoft.firebaseio.com/usuarios.json**
